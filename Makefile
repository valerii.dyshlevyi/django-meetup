.PHONY: setup \
		db \
		create1x \
		create10x \
		create100x \

venv/bin/activate: ## alias for virtual environment
	python -m venv venv

setup: venv/bin/activate ## project setup
	. venv/bin/activate; pip install pip==21.0.1 wheel setuptools
	. venv/bin/activate; pip install --exists-action w -Ur requirements.txt

db: venv/bin/activate ## Run migrations
	. venv/bin/activate; python manage.py migrate

create1x: venv/bin/activate ## Create 1X test data
	. venv/bin/activate; python manage.py create_many_objects --model_name=client --count=100
	. venv/bin/activate; python manage.py create_many_objects --model_name=driver --count=50
	. venv/bin/activate; python manage.py create_many_objects --model_name=order_type --count=30
	. venv/bin/activate; python manage.py create_many_objects --model_name=vehicle --count=40
	. venv/bin/activate; python manage.py create_many_objects --model_name=order --count=500

create10x: venv/bin/activate ## Create 10X test data
	. venv/bin/activate; python manage.py create_many_objects --model_name=client --count=1000
	. venv/bin/activate; python manage.py create_many_objects --model_name=driver --count=500
	. venv/bin/activate; python manage.py create_many_objects --model_name=order_type --count=300
	. venv/bin/activate; python manage.py create_many_objects --model_name=vehicle --count=400
	. venv/bin/activate; python manage.py create_many_objects --model_name=order --count=5000

create100x: venv/bin/activate ## Create 100X test data
	. venv/bin/activate; python manage.py create_many_objects --model_name=client --count=10000
	. venv/bin/activate; python manage.py create_many_objects --model_name=driver --count=5000
	. venv/bin/activate; python manage.py create_many_objects --model_name=order_type --count=3000
	. venv/bin/activate; python manage.py create_many_objects --model_name=vehicle --count=4000
	. venv/bin/activate; python manage.py create_many_objects --model_name=order --count=50000