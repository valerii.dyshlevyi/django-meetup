# Django meetup 

### Requirements:
- Docker [[install]](https://docs.docker.com/install/)
- PostgreSQL v12
- pyenv is required

## Local setup

Check .python-version for required Python version. Best practice is use pyenv.

Run setup:
```bash
make setup
```

## Local environment

First of all you need to create `django_meetup/settings_local.py`:
```bash
touch django_meetup/settings_local.py
```

Then create database user and database itself. Perhaps here you are looking for this commands:

```bash
createdb routing
```

Then run project as simple as:

```bash
make db
```