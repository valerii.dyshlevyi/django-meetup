from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None, **other):
        user = self.model(email=self.normalize_email(email), username=username, **other)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        return self.create_user(
            first_name='Admin',
            last_name='Adminski',
            username=username,
            email=email,
            password=password,
            is_staff=True,
            is_superuser=True,
            address_1="Dnipro, Hlinky steet 2"
        )


class User(AbstractUser):
    secret_code = models.CharField(max_length=16)
    address_1 = models.CharField(max_length=300)
    address_2 = models.CharField(max_length=300, blank=True, default="")

    password_expire_at = models.DateTimeField(null=True, blank=True)

    objects = UserManager()

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"
