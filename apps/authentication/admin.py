from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from . import models


@admin.register(models.User)
class CustomUserAdmin(UserAdmin):
    list_display = (
        "id",
        "username",
        "email",
        "first_name",
        "last_name",
        "is_active",
        "is_staff",
    )
    add_fieldsets = (
        (None, {
            "classes": ("wide",),
            "fields": ("username", "email", "password1", "password2", "address_1",),
        }),
    )

    fieldsets = (
        ("User data", {"fields": (
            "username",
            "password",
        )}),
        ("Personal info", {"fields": (
            "email",
            "first_name",
            "last_name",
            "address_1",
            "address_2",

        )}),
        ("Permissions", {"fields": (
            "is_staff",
            "is_superuser",
        )}),
    )

    list_filter = (
        "is_staff",
        "is_superuser",
        "is_active",
    )

    def get_actions(self, request):
        actions = super(CustomUserAdmin, self).get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions

    def has_delete_permission(self, request, obj=None):
        return False
