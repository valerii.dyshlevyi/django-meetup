from django.apps import AppConfig


class RoutingConfig(AppConfig):
    name = 'apps.routing'
