from rest_framework.routers import DefaultRouter

from .views import (
    ClientViewSet,
    DriverViewSet,
    OrderViewSet,
    OrderTypeViewSet,
    VehicleViewSet,
)

router = DefaultRouter()
router.register('clients', ClientViewSet)
router.register('drivers', DriverViewSet)
router.register('order-types', OrderTypeViewSet)
router.register('orders', OrderViewSet)
router.register('vehicles', VehicleViewSet)

urlpatterns = [] + router.urls
