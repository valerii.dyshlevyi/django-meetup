from django.contrib import admin

from apps.routing import models


@admin.register(models.Driver)
class DriverAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "first_name",
        "last_name",
    )


@admin.register(models.Vehicle)
class VehicleAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "model",
        "make",
        "vin",
    )


@admin.register(models.OrderType)
class OrderTypeAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "kind",
        "max_clients",
    )


@admin.register(models.Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "first_name",
        "last_name",
        "middle_name",
    )


@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "order_status",
        "order_type",
        "driver",
        "vehicle",
        "distance",
        "approx_price",
        "final_price",
    )

    raw_id_fields = (
        "driver",
        "vehicle",
        "order_type",
        "clients"
    )

    list_filter = (
        "order_status",
    )

    list_select_related = (
        "driver",
        "vehicle",
        "order_type",
    )
