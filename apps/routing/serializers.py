from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Client, Driver, OrderType, Order, Vehicle


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = (
            "id",
            "first_name",
            "last_name",
            "middle_name",
        )


class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = (
            "id",
            "first_name",
            "last_name",
        )


class OrderTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderType
        fields = (
            "id",
            "name",
            "kind",
            "max_clients",
        )


class OrderCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = (
            "id",
            "order_status",
            "order_type",
            "clients",
            "driver",
            "vehicle",
            "pick_up_address",
            "drop_off_address",
            "distance",
            "approx_price",
            "final_price",
        )

    def validate_clients(self, clients):
        if len(clients) == 0:
            raise ValidationError("Unable to create order without clients.")
        return clients


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = (
            "id",
            "make",
            "model",
            "vin",
        )


class OrderSerializer(serializers.ModelSerializer):
    clients_details = ClientSerializer(source="clients", many=True, read_only=True)
    driver_detail = DriverSerializer(source="driver", read_only=True)
    vehicle_detail = VehicleSerializer(source="vehicle", read_only=True)
    order_type_detail = OrderTypeSerializer(source="order_type", read_only=True)
    approx_price = serializers.FloatField()
    final_price = serializers.FloatField()

    class Meta:
        model = Order
        fields = (
            "id",
            "order_status",
            "order_type",
            "order_type_detail",
            "clients",
            "clients_details",
            "driver",
            "driver_detail",
            "vehicle",
            "vehicle_detail",
            "pick_up_address",
            "drop_off_address",
            "distance",
            "approx_price",
            "final_price",
            "updated_at",
            "created_at",
        )
