from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import Client, Driver, Order, OrderType, Vehicle
from .serializers import ClientSerializer, DriverSerializer, OrderSerializer, VehicleSerializer


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def destroy(self, request, *args, **kwargs):
        """Method not allowed."""
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class DriverViewSet(ModelViewSet):
    queryset = Driver.objects.all()
    serializer_class = DriverSerializer

    def destroy(self, request, *args, **kwargs):
        """Method not allowed."""
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get_queryset(self):
        """Return filtered queryset."""
        return super().get_queryset().select_related(
            "driver",
            "vehicle",
            "order_type",
        ).prefetch_related(
            "clients"
        )

    def destroy(self, request, *args, **kwargs):
        """Method not allowed."""
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class OrderTypeViewSet(ModelViewSet):
    queryset = OrderType.objects.all()
    serializer_class = OrderSerializer

    def destroy(self, request, *args, **kwargs):
        """Method not allowed."""
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class VehicleViewSet(ModelViewSet):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer

    def destroy(self, request, *args, **kwargs):
        """Method not allowed."""
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
