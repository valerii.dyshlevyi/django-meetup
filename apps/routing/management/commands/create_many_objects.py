import csv
import os

from decimal import Decimal
from typing import Optional

from django.core.management import CommandError
from django.core.management.base import BaseCommand
from django.db.models import Count
from faker import Faker
from faker.generator import random

from apps.routing.models import Vehicle, OrderType, Order, Driver, Client
from apps.routing.serializers import (
    ClientSerializer,
    DriverSerializer,
    OrderCreateSerializer,
    OrderTypeSerializer,
    VehicleSerializer,
)


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.faker = Faker()

    def add_arguments(self, parser):
        parser.add_argument("--model_name", type=str, required=True, help="Name of model")
        parser.add_argument("--count", type=int, required=True, help="Number of objects")

    def _prepare_driver_data(self):
        """Generate data for new Driver object."""
        return {
            "first_name": self.faker.first_name(),
            "last_name": self.faker.last_name(),
        }

    def _prepare_client_data(self):
        """Generate data for new Client object."""
        data = self._prepare_driver_data()
        data.update({"middle_name": self.faker.first_name()})
        return data

    def _get_random_row(self, model):
        """Get random row from database."""
        count = model.objects.aggregate(count=Count('id'))['count']
        random_index = random.randint(0, count - 1)
        return model.objects.all()[random_index]

    def _prepare_order_data(self):
        """Generate data for new Order object."""
        clients_ids = []
        driver: Optional[Driver] = None
        vehicle: Optional[Vehicle] = None
        order_type: OrderType = self._get_random_row(model=OrderType)

        while len(clients_ids) < order_type.max_clients:
            client = self._get_random_row(model=Client)
            if client.id not in clients_ids:
                clients_ids.append(client.id)

        status = random.choice([k for k, v in Order.Statuses.CHOICES])
        if status not in (Order.Statuses.NEW, Order.Statuses.SEARCH):
            driver = self._get_random_row(model=Driver)
            vehicle = self._get_random_row(model=Vehicle)
        approx_price = random.uniform(1.0, 25.0)
        final_price = approx_price + random.uniform(1.0, 5.0)

        return {
            "clients": clients_ids,
            "vehicle": vehicle.id if vehicle else None,
            "driver": driver.id if driver else None,
            "order_status": status,
            "order_type": order_type.id,
            "pick_up_address": self.faker.profile().get("address", ""),
            "drop_off_address": self.faker.profile().get("address", ""),
            "distance": round(random.uniform(1.0, 25.0), 3),
            "approx_price": round(approx_price, 2),
            "final_price": round(final_price, 2),
        }

    def _generate_unique_name(self) -> str:
        """Generate unique name for new OrderType object."""
        new_name = self.faker.simple_profile().get("username")
        if OrderType.objects.filter(name=new_name).exists():
            return self._generate_unique_name()
        return new_name

    def _prepare_order_type_data(self) -> dict:
        """Generate data for new OrderType object."""
        return {
            "name": self._generate_unique_name(),
            "kind": random.choice(["CENTER", "DOWNTOWN", "ALL"]),
            "max_clients": random.randrange(1, 9),
        }

    def _generate_unique_vin(self) -> str:
        """Generate unique vin for Vehicle object."""
        new_vin = self.faker.ean13(prefixes=("0000001", "0000002", "0000003"))
        if Vehicle.objects.filter(vin=new_vin).exists():
            return self._generate_unique_vin()
        return new_vin

    def _prepare_vehicle_data(self) -> dict:
        """Generate data for Vehicle object."""

        return {
            "model": self.faker.license_plate(),
            "make": self.faker.ssn(),
            "vin": self._generate_unique_vin(),
        }

    def handle(self, *args, **options):
        model_name, count = options.get("model_name"), options.get("count", 0)
        classes_mapping = {
            "client": ClientSerializer,
            "driver": DriverSerializer,
            "order_type": OrderTypeSerializer,
            "order": OrderCreateSerializer,
            "vehicle": VehicleSerializer,
        }
        if model_name not in classes_mapping:
            raise CommandError(f"Model name '{model_name}' is not valid.")
        if count < 1 or count > 50000:
            raise CommandError("Please, choose count between 1 and 50000.")

        # Get serializer class
        serializer_class = classes_mapping[model_name]
        created_count = 0
        for step in range(count):
            data = getattr(self, f"_prepare_{model_name}_data")()
            print(data)
            serializer = serializer_class(data=data)
            if not serializer.is_valid():
                raise CommandError(serializer.errors)
            serializer.save()
            created_count += 1

        self.stdout.write(
            "{count} objects of {name} model were successfully created!".format(
                count=created_count,
                name=model_name
            )
        )
