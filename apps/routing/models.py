from django.core.validators import MinValueValidator, MaxValueValidator, MinLengthValidator
from django.db import models


class Client(models.Model):
    first_name = models.CharField(max_length=50, validators=[MinLengthValidator(2)])
    last_name = models.CharField(max_length=50, validators=[MinLengthValidator(1)])
    middle_name = models.CharField(max_length=50, blank=True, default="")

    class Meta:
        verbose_name = "Client"
        verbose_name_plural = "Clients"
        ordering = ("id", )

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Driver(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    class Meta:
        verbose_name = "Driver"
        verbose_name_plural = "Drivers"
        ordering = ("id", )

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Vehicle(models.Model):
    model = models.CharField(max_length=50)
    make = models.CharField(max_length=50)
    vin = models.CharField(max_length=20, unique=True, db_index=True)

    class Meta:
        verbose_name = "Vehicle"
        verbose_name_plural = "Vehicles"
        ordering = ("id", )

    def __str__(self):
        return f"{self.make} {self.model}"


class OrderType(models.Model):
    class Kinds:
        CENTER, DOWNTOWN, ALL = "CENTER", "DOWNTOWN", "ALL"
        CHOICES = (
            (CENTER, CENTER),
            (DOWNTOWN, DOWNTOWN),
            (ALL, ALL),
        )
    name = models.CharField(max_length=30, unique=True, db_index=True)
    kind = models.CharField(max_length=10, choices=Kinds.CHOICES, default=Kinds.CENTER)
    # Max number of clients in vehicle
    max_clients = models.PositiveSmallIntegerField(
        default=1,
        validators=[MinValueValidator(0), MaxValueValidator(100)]
    )

    class Meta:
        verbose_name = "Order Type"
        verbose_name_plural = "Order Types"
        ordering = ("id", )

    def __str__(self):
        return f"{self.kind}: max {self.max_clients}"


class Order(models.Model):
    class Statuses:
        NEW, SEARCH, TO_PICK_UP = "NEW", "SEARCH", "TO_PICK_UP"
        WAITING, TO_DROP_OFF, FINISHED = "WAITING", "TO_DROP_OFF", "FINISHED"
        CHOICES = (
            (NEW, NEW),
            (SEARCH, SEARCH),
            (TO_PICK_UP, TO_PICK_UP),
            (WAITING, WAITING),
            (TO_DROP_OFF, TO_DROP_OFF),
            (FINISHED, FINISHED),
        )
    order_status = models.CharField(max_length=15, choices=Statuses.CHOICES, default=Statuses.NEW)
    order_type = models.ForeignKey(
        OrderType,
        related_name="order_type_orders",
        on_delete=models.CASCADE,
    )
    driver = models.ForeignKey(
        Driver,
        null=True,
        blank=True,
        related_name="driver_orders",
        on_delete=models.CASCADE,
    )
    vehicle = models.ForeignKey(
        Vehicle,
        null=True,
        blank=True,
        related_name="vehicle_orders",
        on_delete=models.CASCADE,
    )
    clients = models.ManyToManyField(Client, blank=True)

    pick_up_address = models.CharField(max_length=300)
    drop_off_address = models.CharField(max_length=300)
    distance = models.FloatField()

    approx_price = models.DecimalField(max_digits=6, decimal_places=2)
    final_price = models.DecimalField(max_digits=6, decimal_places=2)

    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    # here we can place some geo points

    class Meta:
        verbose_name = "Order"
        verbose_name_plural = "Orders"
        ordering = ("id", )

    def __str__(self):
        return f"Order #{self.id}"
